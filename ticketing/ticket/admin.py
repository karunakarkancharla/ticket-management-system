from django.contrib import admin
from .models import Ticket

# Register your models here.

class TicketAdmin(admin.ModelAdmin):
    list_display = ('ticket_number','title','description','created_by','date_created','assigned_to','is_resolved','accepted_date','closed_date','ticket_status')
admin.site.register(Ticket, TicketAdmin)