function validatefun()
{
    var username = document.getElementById("username").value;
    var password = document.getElementById("exampleInputPassword1").value;
    var confirmpassword = document.getElementById("confirmpassword").value;
    if (username == "" && password == "" && confirmpassword =="")
    {
        alert("Please fill all the fields");
        return false
    }
    if (password !== confirmpassword)
    {
        alert("both the password did not match");
        return false
    }
    var uppercaseletters = /[A-Z]/;
    var lowercaseletters = /[a-z]/;
    var speciallsymbols = /[!@#$%^&*()_+{}\[\]:;<>,.?~\\-]/;
    var numbers = /[0-9]/;
    if(password.length < 8)
    {
        alert("Password should be atleast 8 characters long")
    }
        
    if(!uppercaseletters.test(password))
    {
        alert("Password must contain atleast 1Uppercase letter");
        return false
    }
    if(!lowercaseletters.test(password))
    {
        alert("Password must contain atleast 1Lowercase letter");
        return false
    }
    if(!speciallsymbols.test(password))
    {
        alert("Password must contain atleast 1Specialsymbols letter");
        return false
    }
    return true
}

