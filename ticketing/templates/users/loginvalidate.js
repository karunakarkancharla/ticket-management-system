function loginvalidate()
{
    var username = document.getElementById("exampleInputEmail").value;
    var password = document.getElementById("exampleInputPassword1").value;
    if (username == "" && password == "")
    {
        alert("Please fill all the fields");
        return false
    }
    var uppercaseletters = /[A-Z]/;
    var lowercaseletters = /[a-z]/;
    var speciallsymbols = /[!@#$%^&*()_+{}\[\]:;<>,.?~\\-]/;
    var numbers = /[0-9]/;
    if(password.length < 8)
    {
        alert("Password should be atleast 8 characters long")
    }
    if(!uppercaseletters.test(password))
    {
        alert("Password must contain atleast 1Uppercase letter");
        return false
    }
    if(!lowercaseletters.test(password))
    {
        alert("Password must contain atleast 1Lowercase letter");
        return false
    }
    if(!speciallsymbols.test(password))
    {
        alert("Password must contain atleast 1Specialsymbols letter");
        return false
    }
    return true
}