from django.contrib.auth.forms import UserCreationForm
from .models import User
from django.core import validators

class RegisterCustomerForm(UserCreationForm):
    class Meta:
        model = User
        fields =['email', 'username','password']

        